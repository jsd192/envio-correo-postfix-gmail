#!/bin/bash

sudo dnf  install make mailx cyrus-sasl-devel cyrus-sasl-plain cyrus-sasl postfix -y

sed -i "/127.0.0.1/ s/$/ $(hostname)/" /etc/hosts
sed -i "/::1/ s/$/ $(hostname)/" /etc/hosts

sed -i "119s/inet_protocols = all/inet_protocols = ipv4/" /etc/postfix/main.cf
sed -i "318s|^|relayhost = [smtp.gmail.com]:587\nsmtp_use_tls = yes\nsmtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt\nsmtp_sasl_auth_enable = yes\nsmtp_sasl_password_maps = hash:/etc/postfix/sasl/passwd\nsmtp_sasl_security_options = noanonymous\n |" /etc/postfix/main.cf
sed -i "746s/^/#/" /etc/postfix/main.cf

mkdir /etc/postfix/sasl
touch /etc/postfix/sasl/passwd

echo "---------------------------------------  "
echo "-- CONFIGURACÓN DE CORREO --"
read -p "Escribe el correo gmail y dale enter: " correo
read -p "Escribe la contraseña del correo: " contra
echo "---------------------------------------  "
echo "[smtp.gmail.com]:587 $correo:$contra" > /etc/postfix/sasl/passwd

postmap /etc/postfix/sasl/passwd
chmod 0600 /etc/postfix/sasl/passwd
chmod 0600 /etc/postfix/sasl/passwd.db

cd /etc/pki/tls/certs/

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout "$HOSTNAME".pem -out "$HOSTNAME".pem -subj "/C=PE/ST=LIMA/L=JESUSMARIA/O=DEXTRE.XYZ/CN=$HOSTNAME"

cat /etc/pki/tls/certs/"$HOSTNAME".pem >> /etc/postfix/cacert.pem

systemctl start postfix
systemctl enable postfix
systemctl restart postfix

echo "---------------------------------------------"
echo "Vamos a probar enviando un correo electronico"
read -p "Escriba el correo destino: " correito
read -p "Asunto del correo: " asunto
read -p "Escribe tu mensaje: " cuerpo

echo "$cuerpo" | mail -s "${asunto}" "${correito}"
echo "REVISANDO EL ESTADO DEL CORREO\n$(tail /var/log/maillog | grep --color 'status=')"  
